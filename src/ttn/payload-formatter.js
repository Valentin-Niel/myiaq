function Decoder(bytes, port) {
    var decoded = {};
    if (port === 1) {
      var temp = (bytes[ 0 ] << 1) | (bytes[ 1 ] >> 7);
      decoded.temperature = temp / 10;

      var hum = bytes[ 1 ] & ~(1 << 7); // val & ~0x80 -> val & 0x7f
      decoded.humidite = hum;

      var co2 = (bytes [ 2 ] << 8) | bytes[ 3 ]; // 2222 2222 0000 0000 | 2222 2222
      decoded.co2 = co2;
    }
    return decoded;
}