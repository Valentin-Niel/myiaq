#ifndef BCM2835WRAPPER_H
#define BCM2835WRAPPER_H

#include <QObject>

#include <bcm2835.h>

class Bcm2835Wrapper
{
public:
    static Bcm2835Wrapper& getInstance();
    virtual ~Bcm2835Wrapper();

    // Rendre inaccessible les constructeurs de copie/déplacement
    // et opérateurs d'affectaiotn/déplacement
    Bcm2835Wrapper(Bcm2835Wrapper const&) =delete;
    Bcm2835Wrapper(Bcm2835Wrapper&&) = delete;
    Bcm2835Wrapper& operator=(Bcm2835Wrapper const&) = delete;
    Bcm2835Wrapper operator=(Bcm2835Wrapper&&) = delete;

    // Module I2C
    typedef enum {STANDARD, FULL, FAST} i2c_clock_t;
    typedef enum {SUCCESS=BCM2835_I2C_REASON_OK
                  , NAK_ERR=BCM2835_I2C_REASON_ERROR_NACK
                  , CLK_STRETCH_ERR=BCM2835_I2C_REASON_ERROR_CLKT
                  , DATA_ERR=BCM2835_I2C_REASON_ERROR_DATA
                 } i2c_err_t;
    void i2c_setClock(i2c_clock_t clock);
    i2c_err_t i2c_write(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_read(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_write_read_rs(uint8_t addr, uint8_t* cmd, int cmdLen, uint8_t* buf, int bufLen);


private:
    static const uint16_t _CLK_DIV = BCM2835_I2C_CLOCK_DIVIDER_2500;
    Bcm2835Wrapper();
};

#endif // BCM2835WRAPPER_H
