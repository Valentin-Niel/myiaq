#include <math.h>
#include <QTime>
#include <QDebug>

#include "daqapp.h"

DaqApp::DaqApp(QObject *parent) : QObject(parent)
{
    connect(&_sensors, &IDataFeed::stateChanged, this, &DaqApp::_onStateChanged);

    _cron.setInterval(2500 /*ms*/);
    connect(&_cron, &QTimer::timeout, this, &DaqApp::_onTimeout);
}


void DaqApp::_onTimeout()
{
    // Acquisition taux C02, T°C, HR%
    _scd30.readSensor(_t, _hr, _co2);

    // Acquisition luminosité
    _veml7700.readAmbientLight(_lux);

//    _lux.addSample(als);

    QJsonObject T = {
        {"val", isnan(_t) ? QJsonValue(QJsonValue::Null) : _t}
        , {"unit", "°C"}
    };

    QJsonObject HR = {
        {"val", isnan(_hr) ? QJsonValue(QJsonValue::Null) : _hr}
        , {"unit", "%"}
    };

    QJsonObject CO2 = {
        {"val", isnan(_co2) ? QJsonValue(QJsonValue::Null) : _co2}
        , {"unit", "ppm"}
    };

    QJsonObject LUX = {
        {"val", isnan(_lux) ? QJsonValue(QJsonValue::Null) : _lux}
        , {"unit", "lux"}
    };

    QJsonObject sensorsValue = {
        {"T"  , T}
        , {"HR", HR}
        , {"CO2", CO2}
        , {"LUX", LUX}
    };

    // Publication des données au format JSON
    _sensors.push(sensorsValue);
}

void DaqApp::_onStateChanged(IDataFeed::FeedState state) {
    switch(state) {
        case IDataFeed::Ready:
            qDebug() << "cron start";
            _cron.start();
            break;
        case IDataFeed::Error:
        case IDataFeed::Busy :
            _cron.stop();
            break;
    }
}
