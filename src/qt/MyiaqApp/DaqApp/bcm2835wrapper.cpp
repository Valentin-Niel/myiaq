#include <QDebug>

#include <bcm2835.h>

#include "bcm2835wrapper.h"

Bcm2835Wrapper &Bcm2835Wrapper::getInstance()
{
    static Bcm2835Wrapper instance;
    return instance;
}

Bcm2835Wrapper::~Bcm2835Wrapper()
{
    bcm2835_i2c_end();
    bcm2835_close();
}

void Bcm2835Wrapper::i2c_setClock(Bcm2835Wrapper::i2c_clock_t clock)
{
    int br;

    switch(clock) {
    case Bcm2835Wrapper::STANDARD :
        br = 100000;
        break;
    case Bcm2835Wrapper::FULL :
        br = 400000;
        break;
    case Bcm2835Wrapper::FAST :
        br = 1000000;
        break;
    default:
        br = -1;
        break;
    }

    if(br > 0) {
        bcm2835_i2c_set_baudrate(br);
    }
}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_read(uint8_t addr, uint8_t *buf, int len)
{
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_read(reinterpret_cast<char*>(buf), len));

    return status;
}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_write_read_rs(uint8_t addr, uint8_t *cmd, int cmdLen, uint8_t *buf, int bufLen)
{
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_write_read_rs(
        reinterpret_cast<char*>(cmd)
        , cmdLen
        , reinterpret_cast<char*>(buf)
        , bufLen
        ));

    return status;

}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_write(uint8_t addr, uint8_t* buf, int len) {
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_write(reinterpret_cast<char*>(buf), len));

    return status;
}

Bcm2835Wrapper::Bcm2835Wrapper()
{
    if(bcm2835_init() != 1) {
        qDebug() << "Erreur initialisation librairie bcm2835";
    } else {
        if( bcm2835_i2c_begin() != 1) {
            qDebug() << "Erreur initialisation bus I2C librairie bcm2835. Programme exécuté sans `sudo` ?";
        } else {
            bcm2835_i2c_setClockDivider(_CLK_DIV);
        }
    }

}
