#include <math.h>
#include <QThread>
#include <QDebug>

#include "scd30.h"

Scd30::Scd30(QObject *parent) : QObject(parent)
  , _bcm(Bcm2835Wrapper::getInstance())
{

}

/**
 * @brief Scd30::readSensor
 * @param theTemp
 * @param theHum
 * @param theCo2
 * @return 0 si OK
 * @return -1 si erreur écriture i2c
 * @return -2 si erreur lecture i2c
 */
int Scd30::readSensor(double& theTemp, double& theHum, double& theCo2)
{
    uint8_t resp[ 18 ] = {
        0x44, 0x26      // CO2 MSB      -+
        , 0xab          // CO2 MSB CRC   |=> 665ppm
        , 0x61 , 0xc5   // CO2 LSB       |
        , 0x92          // C02 LSB CRC  -+
        , 0x41, 0xbc    // T° MSB       -+
        , 0x3e          // T° MSB CRC    |=> 23°C
        , 0x9d, 0x1c    // T° LSB        |
        , 0xb2          // T° LSB CRC   -+
        , 0x41, 0xb9    // HR% MSB      -+
        , 0xcb          // HR% MSB CRC   |=> 23%
        , 0xb7, 0xc0    // HR% LSB       |
        , 0xf9          // HR% LSB CRC  -+
    };

    uint8_t readMeasurementCmd[] = {0x03, 0x00};

    theTemp = theHum = theCo2 = nan(""); // initialise les valeurs avec NaN (Not a Number)

    if( _bcm.i2c_write(_SCD30_ADDR, readMeasurementCmd, 2) != Bcm2835Wrapper::SUCCESS) {
        qDebug() << "Erreur écriture sur capteur SCD30";
        return -1;
    }

    QThread::msleep(3); //

    if( _bcm.i2c_read(_SCD30_ADDR, resp, 18) != Bcm2835Wrapper::SUCCESS) {
        qDebug() << "Erreur lecture sur capteur SCD30";
        return -2;
    }

    uint32_t ieee754;

    ieee754 = static_cast<uint32_t>(
        static_cast<uint32_t>(resp[ 0 ] << 24)
        | static_cast<uint32_t>(resp[ 1 ] << 16)
        | static_cast<uint32_t>(resp[ 3 ] << 8)
        | static_cast<uint32_t>(resp[ 4 ] << 0)
    );

    theCo2 = static_cast<double>(*(reinterpret_cast<float *>(&ieee754)));

    ieee754 = static_cast<uint32_t>(
        static_cast<uint32_t>(resp[ 6 ] << 24)
        | static_cast<uint32_t>(resp[ 7 ] << 16)
        | static_cast<uint32_t>(resp[ 9 ] << 8)
        | static_cast<uint32_t>(resp[ 10 ] << 0)
    );

    theTemp = static_cast<double>(*(reinterpret_cast<float *>(&ieee754)));

    ieee754 = static_cast<uint32_t>(
        static_cast<uint32_t>(resp[ 12 ] << 24)
        | static_cast<uint32_t>(resp[ 13 ] << 16)
        | static_cast<uint32_t>(resp[ 15 ] << 8)
        | static_cast<uint32_t>(resp[ 16 ] << 0)
    );

    theHum = static_cast<double>(*(reinterpret_cast<float *>(&ieee754)));

    return 0;
}
