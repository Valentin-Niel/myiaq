#ifndef LORAAPP_H
#define LORAAPP_H

#include <QObject>
#include <QTimer>

#include "sensorsdatafeed.h"
#include "cubecell.h"

class LoraApp : public QObject
{
    Q_OBJECT
public:
    explicit LoraApp(QObject *parent = nullptr);

private:
    QHash<QString&, double> _sensors_values;
    SensorsDataFeed _sensors;
    TTN* _ttn;
    QByteArray _makePayload(QJsonObject& mqttTopicValue);
    const QString _SERIAL_PORT = "/dev/ttyAMA0"; /* RPi liaison série sur GPIOs */
//   const QString _SERIAL_PORT = "/dev/ttyUSB0"; /* RPi liaison série sur USB */
//    const QString _SERIAL_PORT = "COM7"; /* Windows */
//    const QString _SERIAL_PORT = "COM9"; /* Windows port serie virtuel */
    QByteArray _loraPayload;
    QTimer _tmr;

signals:

private slots:
    //void _onStateChanged(IDataFeed::FeedState state);
    void _onSensorsDataReady();
    void _onTtnJoined();
    void _onUplinkTtnDone();
    void _onTmrTimeout();
};

#endif // LORAAPP_H
