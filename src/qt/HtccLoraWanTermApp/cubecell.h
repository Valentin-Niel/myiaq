﻿#ifndef CUBECELL_H
#define CUBECELL_H

#include <QObject>
#include <QVector>

#include "cubecellworker.h"

class Cubecell : public QObject
{
    Q_OBJECT
public:
    explicit Cubecell(QString serialPort, QObject *parent = nullptr);
    void wakeUp();
    void getChipID();
    void getDevEui();
    void getAppEui();
    void getAppKey();
    void getDutyCycle();
    void join();
    void sendHex(QString data);
    void isTxConfirmed(bool enabled = true);

private :
    QString _portName;
    CubecellWorker _workerThread;
    enum {WakeUp, ChipID, DevEui, AppEui, AppKey, DutyCycle, Join, WaitJoin, SendHex, WaitSendHex} idCmde;
    QVector<QString>_ATCmdes = {
        "AT+XXX"
        , "AT+ChipID="
        , "AT+DevEui="
        , "AT+AppEui="
        , "AT+AppKey="
        , "AT+DutyCycle="
        , "AT+Join="
        , "" /* Commande vide pour se contenter d'attendre le status du JOIN */
        , "AT+SendHex="
        , "" /* Commande vide pour se contenter d'attendre le status du SEND_HEX */
    };
    bool hasHexDigits(const QString &srcStr, QString &hexStr, int len);
    bool hasDecDigits(const QString &srcStr, QString &decStr);

signals:
    void chipIdReady(const QString & s);
    void devEuiReady(const QString & s);
    void appEuiReady(const QString & s);
    void appKeyReady(const QString & s);
    void dutyCycleReady(const QString & s);
    void joinReady(const QString & s);
    void sendHexReady(const QString & s);

private slots:
    void onResponseReceived(const int &id, const QString &s);
    void onTimeOut(const int &id, const QString &err);

};

#endif // CUBECELL_H
